// +build windows,amd64

package main

// is64BitOS returns if the os is 64 bit. On 64 bit executable
// this will allways be true (see windows_386.go)
func is64BitOS() (bool, error) {
	return true, nil
}
