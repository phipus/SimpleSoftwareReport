package main

// InstalledSoftware contains information about an installed software
type InstalledSoftware struct {
	DisplayName    string `json:"display_name"`
	DisplayVersion string `json:"display_version"`
	Publisher      string `json:"publisher"`
}

// ComputerInfo contains information about a computer
type ComputerInfo struct {
	Name        string `json:"name"`
	CurrentUser string `json:"current_user"`
}

// Report is the data reported to the server
type Report struct {
	ComputerInfo      `json:"computer_info"`
	InstalledSoftware []InstalledSoftware `json:"installed_software"`
	ReportVersion     string              `json:"report_version"`
}

// readReport generates a new report
func readReport(report *Report) (err error) {
	err = getComputerInfo(&report.ComputerInfo)
	if err != nil {
		return
	}

	report.InstalledSoftware, err = getSoftware()
	if err != nil {
		return
	}
	report.ReportVersion = "1.0"
	return
}
