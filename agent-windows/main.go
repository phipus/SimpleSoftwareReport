package main

import (
	"errors"
	"fmt"
	"log"
	"os"
)

type cliArgs struct {
	Action string
	Server *string
}

var errInvalidArgs = errors.New("Invalid args")

func parseArgs(args *cliArgs) (err error) {
	if len(os.Args) < 2 {
		return errInvalidArgs
	}
	args.Action = os.Args[1]
	if !(args.Action == "PRINT" || args.Action == "SEND") {
		return errInvalidArgs
	}
	if args.Action == "SEND" {
		if len(os.Args) < 4 {
			return errInvalidArgs
		}
		if os.Args[2] != "--server" {
			return errInvalidArgs
		}
		args.Server = &os.Args[3]
	}
	return
}

func main() {
	var args cliArgs
	err := parseArgs(&args)
	if err == errInvalidArgs {
		fmt.Fprintln(os.Stderr, "Useage agent.exe SEND|PRINT [--server SERVER]")
		os.Exit(1)
		return
	} else if err != nil {
		panic(err)
	}
	var report Report
	err = readReport(&report)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create report (%s)", err.Error())
		os.Exit(2)
		return
	}
	if args.Action == "PRINT" {
		err = writeReport(&report, os.Stdout)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else if args.Action == "SEND" {
		err = sendReport(&report, *args.Server)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to send report (%s)", err.Error())
			return
		}
	}
}
