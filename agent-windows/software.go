// +build windows,amd64 windows,386

package main

import (
	"errors"
	"os"

	"golang.org/x/sys/windows/registry"
)

var errInvalidEntry = errors.New("Invalid entry")

func readRegistryString(key registry.Key, name string, required bool) (value string, err error) {
	value, _, err = key.GetStringValue(name)
	if (err == os.ErrNotExist || err == registry.ErrNotExist) && !required {
		err = nil
	}
	return
}

func readRegistryInteger(key registry.Key, name string, required bool) (value uint64, err error) {
	value, _, err = key.GetIntegerValue(name)
	if (err == os.ErrNotExist || err == registry.ErrNotExist) && !required {
		err = nil
	}
	return
}

func readRegistryInstallation(key registry.Key, inst *InstalledSoftware) (err error) {
	inst.DisplayName, err = readRegistryString(key, "DisplayName", true)
	if err != nil {
		return
	}
	releaseType, err := readRegistryString(key, "ReleaseType", false)
	if err != nil {
		return
	}
	if releaseType != "" {
		return errInvalidEntry
	}
	systemComponent, err := readRegistryInteger(key, "SystemComponent", false)
	if err != nil {
		return
	}
	if systemComponent != 0 {
		return errInvalidEntry
	}
	parentKeyName, err := readRegistryString(key, "ParentKeyName", false)
	if err != nil {
		return
	}
	if parentKeyName != "" {
		return errInvalidEntry
	}
	inst.Publisher, err = readRegistryString(key, "Publisher", false)
	if err != nil {
		return
	}
	inst.DisplayVersion, err = readRegistryString(key, "DisplayVersion", false)
	if err != nil {
		return
	}
	return
}

func readRegistryInstalledSoftware(key registry.Key) (installs []InstalledSoftware, err error) {
	subKeyNames, err := key.ReadSubKeyNames(0)
	if err != nil {
		return nil, err
	}
	installs = make([]InstalledSoftware, 0, len(subKeyNames))
	var subKey registry.Key
	var inst InstalledSoftware
	for _, subKeyName := range subKeyNames {
		subKey, err = registry.OpenKey(key, subKeyName, registry.READ)
		if err != nil {
			return nil, err
		}
		defer subKey.Close()
		err = readRegistryInstallation(subKey, &inst)
		if err != nil {
			err = nil
			continue
		}

		installs = append(installs, inst)
	}

	return
}

func getSoftware() (installs []InstalledSoftware, err error) {
	const baseKeyName = `SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall`
	key32, err := registry.OpenKey(registry.LOCAL_MACHINE, baseKeyName, registry.WOW64_32KEY|registry.READ)
	if err != nil {
		return nil, err
	}
	defer key32.Close()

	installs, err = readRegistryInstalledSoftware(key32)
	if err != nil {
		return
	}

	is64Bit, err := is64BitOS()
	if err != nil || !is64Bit {
		return
	}

	// We run a 64 bit windows, get the 64 bit registry hive
	key64, err := registry.OpenKey(registry.LOCAL_MACHINE, baseKeyName, registry.WOW64_64KEY|registry.READ)
	if err != nil {
		return
	}
	defer key64.Close()

	sw64, err := readRegistryInstalledSoftware(key64)
	if err != nil {
		return
	}
	installs = append(installs, sw64...)
	return
}
