# Simple Software Reports
Simple Software Reports is a simple web-application to monitor software installations on 
computers. Currently only windows and MacOS computers are supported. Simple Software Reports has a 
client / server based architecture. The server has a web-gui to view the 
reports. On the clients, an agent installation is needed.

# Why this tool?
I wrote this tool to simplify my work. Our Client Management System is bad, especially in
generating reports. First I wrote this tool as an extension to our CMS, but as time went by
it was easier to write a seperate agent instead of relying to the CMSs agent.

# Is this tool ready to use
It seems to work, but I can't make any promise. If you want to use it, you schould probably
be familliar with golang and Python. 

# Server installation
```bash
# clone the repository
git clone https://gitlab.com/sphipu/SimpleSoftwareReport.git

# Change into the server direcotry
cd SimpleSoftwareReport/server

# create a virtual environment (python 3.5 needed)
virtualenv flask

# activate the environment (Linux)
source flask/bin/activate

# activate the environment (Windows)
call flask\Scripts\activate.bat

# Install the requirements
pip install -r requirements.txt

# Initialize everything
flask configure
flask init-db

# Run (in debug mode)
flask run
```

For a production environment, please consither one of the following
hosting methods: http://flask.pocoo.org/docs/0.12/deploying/#self-hosted-options

# Agent Installation
The Agent is only supported for windows and mac yet. To install the agent, copy
the corresponding agent file for windows or mac to a location and configure a 
schedulet task / cron job to run the file with the following arguments:

    # MacOS (python >= 3.5 is required)
    agent.py SEND --server <server_name_or_address>

    # Windows
    agent.exe SEND --server <server_name_or_address>


