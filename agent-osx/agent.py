#!/usr/bin/env python3
"""
Simple Software Reports agent for MacOS
This scripts scans the /Application folder and parses the info.plist file of
every .app bundle (recursive).

If the action is set to SEND, an url of the server-instance must be specified.
"""
import os
import plistlib
import pwd
import argparse
import json
import sys
from urllib.request import Request, urlopen


CONFIG = {
    'VERBOSE': False,
}


class ComputerInfo:
    """Information about a local computer"""
    def __init__(self, name, current_user):
        self.name = name
        self.current_user = current_user

    @classmethod
    def from_localhost(cls):
        """Get ComputerInfo from localhost"""
        name = os.uname().nodename
        current_user = pwd.getpwuid(os.stat('/dev/console').st_uid).pw_name
        return cls(name, current_user)

    def to_dict(self):
        return {'name': self.name, 'current_user': self.current_user}



class Report:
    def __init__(self, computer_info, installed_software):
        self.computer_info = computer_info
        self.installed_software = installed_software
        self.report_version = '1.0'


    @classmethod
    def from_localhost(cls):
        computer_info = ComputerInfo.from_localhost()
        installed_software = list(SoftwareBundle.get_bundles())
        return cls(computer_info, installed_software)


    def to_dict(self):
        return {
            'computer_info': self.computer_info.to_dict(),
            'installed_software': [i.to_dict() for i in self.installed_software],
            'report_version': self.report_version,
        }



class SoftwareBundle:
    class _InvalidBundleError(ValueError):
        pass


    def __init__(self, display_name, display_version, publisher):
        self.display_name = display_name
        self.display_version = display_version
        self.publisher = publisher


    @classmethod
    def from_plist(cls, path):
        with open(path, 'rb') as plist:
            info = plistlib.load(plist)
            name = info.get('CFBundleName') or info.get('CFBundleExecutable')
            if name is None:
                raise cls._InvalidBundleError
            try:
                version = info['CFBundleShortVersionString']
            except KeyError as err:
                raise cls._InvalidBundleError from err
            return cls(name, version, '')


    def to_dict(self):
        return {'display_name': self.display_name,
                'display_version': self.display_version,
                'publisher': self.publisher}


    @classmethod
    def get_bundles(cls, root_dir='/Applications'):
        for path in os.scandir(root_dir):
            if path.is_dir():
                if path.name.endswith('.app'):
                    plist_name = os.path.join(path.path, 'Contents',
                                              'info.plist')
                    try:
                        yield cls.from_plist(plist_name)
                    except cls._InvalidBundleError:
                        if CONFIG['VERBOSE']:
                            message = '%s is an invalid plist file' % plist_name
                            print(message, file=sys.stderr)
                else:
                    yield from cls.get_bundles(path.path)
            elif CONFIG['VERBOSE']:
                print('Ignore file %s' % path.path, file=sys.stderr)



def send_report(report, server):
    headers = {'Content-Type': 'application/json'}
    data = json.dumps(report.to_dict()).encode()
    url = server + ('' if server.endswith('/') else '/') + 'api/1.0/save_report'
    request = Request(url, data, headers=headers)

    with urlopen(request) as response:
        status = response.getcode()
        if status < 200 or status >= 300:
            if CONFIG['VERBOSE']:
                print(response.body, file=sys.stderr)
            msg = 'Server returned an invalid status %s' % status
            raise RuntimeError(msg)


def print_report(report):
    print('Computer Name:', report.computer_info.name)
    print('Current User:', report.computer_info.current_user)
    print('Installed Software:')
    for software in report.installed_software:
        print('    Display Name:', software.display_name)
        print('    Display Version:', software.display_version)
        print('')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('action', type=str, choices=['SEND', 'PRINT'])
    parser.add_argument('--server', '-s', type=str)
    parser.add_argument('--verbose', '-v', action='store_true')
    args = parser.parse_args()

    CONFIG['VERBOSE'] = args.verbose

    report = Report.from_localhost()

    if args.action == 'SEND':
        if not args.server:
            raise ValueError('server must not be empty if action is send')
        send_report(report, args.server)
    elif args.action == 'PRINT':
        print_report(report)


if __name__ == '__main__':
    main()
