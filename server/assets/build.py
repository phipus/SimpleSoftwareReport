#!/usr/bin/env python3
import os
import sass
import hashlib

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

def scss_file(src_name, dst_name):
    src_path = os.path.join(ROOT_DIR, 'assets', 'styles', src_name)
    dst_path = os.path.join(ROOT_DIR, 'app', 'static', dst_name)
    md5_path = dst_path + '.md5'

    css = sass.compile(filename=src_path, output_style='compressed')
    file_hash = hashlib.md5(css.encode())

    with open(dst_path, 'w') as dst_file:
        dst_file.write(css)
    
    with open(md5_path, 'w') as hash_file:
        hash_file.write(file_hash.hexdigest())
        hash_file.write('\n')


scss_file('style.scss', 'style.css')
