from collections import namedtuple
import enum

from .exceptions import QuerySyntaxError

class TokenType(enum.Enum):
    WORD = 1
    AND = 2
    OR = 3
    NOT = 4
    WHOLE_WORD = 5
    RESPECT_CASE = 6
    OPEN_BRACKET = 8
    CLOSE_BRACKET = 9
    IN = 10
    EOF = 99
     

Token = namedtuple('Token', ['text', 'position', 'type', 'value'])


def _get_token(word, pos):
    word = ''.join(word)
    if word == 'AND':
        token = Token(word, pos, TokenType.AND, None)
    elif word == 'OR':
        token = Token(word, pos, TokenType.OR, None)
    elif word == 'NOT':
        token = Token(word, pos, TokenType.NOT, None)
    elif word == 'WORD':
        token = Token(word, pos, TokenType.WHOLE_WORD, None)
    elif word == 'CASE':
        token = Token(word, pos, TokenType.RESPECT_CASE, None)
    elif word == 'IN':
        token = Token(word, pos, TokenType.IN, None)
    else:
        token = Token(word, pos, TokenType.WORD, word)
    return token
   

def tokenize(text):
    word = []
    quote = None
    start_pos = 0

    for pos, c in enumerate(text):
        if c.isspace():
            if quote:
                word.append(c)
            elif len(word) == 0:
                start_pos = pos + 1
                continue
            else:
                yield _get_token(word, start_pos)
                word.clear()
                start_pos = pos + 1
        elif c in ('"', "'"):
            if c == quote:
                if len(word) > 0:
                    yield _get_token(word, start_pos)
                    word.clear()
                start_pos = pos + 2
                quote = None
            elif quote is None:
                quote = c
        elif c == '(':
            if len(word) > 0:
                yield _get_token(word, start_pos)
                word.clear()
            start_pos = pos + 2
            yield Token(pos, c, TokenType.OPEN_BRACKET, None)
        elif c == ')':
            if len(word) > 0:
                yield _get_token(word, start_pos)
                word.clear()
            start_pos = pos + 2
            yield Token(pos, c, TokenType.CLOSE_BRACKET, None)
        else:
            word.append(c)
    if quote:
        raise QuerySyntaxError("Unexpected EOF at %d" % (pos + 1))

    if len(word) > 0:
        yield _get_token(word, start_pos)
    yield Token('EOF', len(text), TokenType.EOF, None)
