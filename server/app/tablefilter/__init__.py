from .exceptions import QuerySyntaxError
from .query import Query

def match_row(query_str, row, *, column_names=None):
    return Query(query_str, column_names).match_row(row)

def match_word(query_str, word):
    return Query(query_str).match_word(word)


