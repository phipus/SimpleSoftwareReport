from .parser import parse


class Query:
    def __init__(self, text, column_names=None):
        self.expr = parse(text, column_names or {})
    
    def match_word(self, word):
        row = [word]
        return self.expr.evaluate(row)
    
    def match_row(self, row):
        return self.expr.evaluate(row)
    
    def filter_table(self, table):
        for row in table:
            if self.match_row(row):
                yield row
