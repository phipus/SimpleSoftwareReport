CREATE TABLE `computer` (
    `id` INT PRIMARY KEY NOT NULL auto_increment,
	`name` VARCHAR(255) NOT NULL UNIQUE,
    `current_user` TEXT NOT NULL,
    `last_connection` DATETIME NOT NULL
);

CREATE TABLE `software` (
    `id` INT PRIMARY KEY NOT NULL auto_increment,
    `display_name` TEXT NOT NULL,
    `display_version` TEXT NOT NULL,
    `publisher` TEXT NULL,
    `computer_id` INTEGER NOT NULL,
    FOREIGN KEY (`computer_id`) REFERENCES `computer`(`id`)
);

CREATE INDEX `idx_software_computer_id` ON `software`(`id`);
CREATE INDEX `idx_computer_name` ON `computer`(`name`);
