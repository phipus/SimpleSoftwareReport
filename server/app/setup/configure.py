import io
import os
import sys
from flask import current_app



class Option:
    def __init__(self, name, description, default=None, type_=str):
        self.name = name
        self.description = description
        self.default = default
        self.type_ = type_


    def read_input(self):
        if self.default is not None:
            prompt = '%s [%s]: ' % (self.description, self.default)
        else:
            prompt = '%s: ' % self.description

        while 1:
            user_input = input(prompt).strip()
            if user_input:
                return self.type_(user_input)
            elif self.default is not None:
                return None


    def write_to(self, file, value):
        file.write('# %s\n' % self.description)
        if value is None:
            file.write('# %s=%r\n\n' % (self.name, self.default))
        else:
            file.write('%s=%r\n\n' % (self.name, value))


OPTIONS = [
    Option('DB_NAME', 'MySQL database name'),
    Option('DB_USER', 'MySQL database user', 'root'),
    Option('DB_HOST', 'MySQL host', 'localhost'),
    Option('DB_PORT', 'MySQL port', 3306, int),
    Option('DB_PASSWORD', 'MySQL database password'),
    Option('DB_CHARSET', 'MySQL database charset', 'utf8mb4'),
    Option('DB_POOL_SIZE', 'MySQL database pool size', 4, int)
]


def configure():
    os.makedirs(current_app.instance_path, exist_ok=True)
    filename = os.path.join(current_app.instance_path, 'config.py')
    buffer = io.StringIO()
    for option in OPTIONS:
        value = option.read_input()
        option.write_to(buffer, value)

    with open(filename, 'w') as envfile:
        envfile.write(buffer.getvalue())
