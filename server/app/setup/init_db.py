import os.path
from flask import current_app

from ..database import new_connection

def read_script(filename):
    with open(filename) as file:
       for qry in file.read().split(';'):
           qry = qry.strip()
           if qry:
               yield qry


def init_db():
    sqlfilename = os.path.join(os.path.dirname(__file__), 'dbschema.sql')
    connection = new_connection(current_app.config)

    try:
        cursor = connection.cursor()
        for stmt in read_script(sqlfilename):
            cursor.execute(stmt)
        connection.commit()
    finally:
        cursor.close()
        connection.close()
    pass