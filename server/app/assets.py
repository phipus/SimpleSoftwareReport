import os.path
from functools import lru_cache
from flask import url_for, current_app



@lru_cache(maxsize=64)
def get_file_hash(filename):
    name = os.path.join(current_app.root_path, 'static', filename + '.md5')
    with open(name, 'r') as file:
        filehash = file.read().strip()
    print(filehash)
    return filehash


def asset_url(filename):
    file_hash = get_file_hash(filename)
    return url_for('static', filename=filename, h=file_hash)

def asset_url_debug(filename):
    file_hash = get_file_hash.__wrapped__(filename)
    return url_for('static', filename=filename, h=file_hash)