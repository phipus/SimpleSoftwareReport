from collections import namedtuple
from flask import request, abort, render_template
from .tablefilter import Query

Row = namedtuple('Row', [
    'computer', 'software', 'version', 'user',
    'publisher', 'last_connection'
])

def get_table(db):
    cur = db.cursor()
    cur.execute(
        "SELECT c.name, s.display_name, "
        "s.display_version, c.current_user, s.publisher, c.last_connection "
        "FROM software AS s LEFT JOIN computer AS c "
        "ON c.id = s.computer_id"
    )
    for row in cur:
        yield Row(*row)

_COLUMN_NAMES = {
    'computer': 0,
    'software': 1,
    'version': 2,
    'user': 3,
    'publisher': 4
}

def lower_itemgetter(index):
    def sort_key(item):
        key = item[index]
        if isinstance(key, str):
            return key.lower()
        return key
    return sort_key


def search_db(db, query_str, sort_key, reverse):
    table = get_table(db)
    query = Query(query_str, _COLUMN_NAMES)
    data = query.filter_table(table)
    result = sorted(data, key=lower_itemgetter(sort_key), reverse=reverse)
    return result


