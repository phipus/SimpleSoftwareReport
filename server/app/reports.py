import sys
from datetime import datetime
from marshmallow import Schema, fields, post_load, ValidationError, validates
from flask import request, abort


class ComputerInfo:
    def __init__(self, name, current_user):
        self.name = name
        self.current_user = current_user


class ComputerInfoSchema(Schema):
    name = fields.String(required=True)
    current_user = fields.String(required=True)

    @post_load
    def make_computer_info(self, data):
        return ComputerInfo(**data)


class SoftwareInstallation:
    def __init__(self, display_name, display_version, publisher):
        self.display_name = display_name
        self.display_version = display_version
        self.publisher = publisher


class SoftwareInstallationSchema(Schema):
    display_name = fields.String(required=True)
    display_version = fields.String(required=True)
    publisher = fields.String(default='')

    @post_load
    def make_software_installation(self, data):
        return SoftwareInstallation(**data)


class ReportSchema(Schema):
    computer_info = fields.Nested(ComputerInfoSchema)
    installed_software = fields.List(fields.Nested(SoftwareInstallationSchema))
    report_version = fields.String(required=True)


    @validates('report_version')
    def validate_report_version(self, value):
        if value != '1.0':
            raise ValidationError('report version must be 1.0')


    @post_load
    def make_report(self, data):
        data.pop('report_version')
        return Report(**data)


class Report:
    def __init__(self, computer_info, installed_software):
        self.computer_info = computer_info
        self.installed_software = installed_software
        self.computer_id = None


    def _insert(self, db):
        cur = db.cursor()
        now = datetime.utcnow()
        cur.execute(
            "INSERT INTO computer VALUES "
            "(NULL, %s, %s, %s)", 
            (self.computer_info.name, self.computer_info.current_user, now))
        self.computer_id = cur.lastrowid
        self._insert_software(db)


    def _insert_software(self, db):
        cur = db.cursor()
        cur.executemany(
            "INSERT INTO software VALUES (NULL, %s, %s, %s, %s)", 
            ([i.display_name, i.display_version, i.publisher, self.computer_id]
              for i in self.installed_software)
        )
    

    def _update(self, db):
        cur = db.cursor()
        now = datetime.utcnow()
        cur.execute(
            "UPDATE computer SET `current_user` = %s, "
            "last_connection = %s "
            "WHERE id = %s", 
            (self.computer_info.current_user, now, self.computer_id,)
        )
        cur.execute("DELETE FROM software WHERE id = %s",
                    (self.computer_id,))
        self._insert_software(db)


    def save(self, db):
        cur = db.cursor()
        cur.execute(
            "SELECT id FROM computer WHERE name = %s",
            (self.computer_info.name,)
        )
        result = cur.fetchone()
        if result is None:
            self._insert(db)
        else:
            self.computer_id = result[0]
            self._update(db)


REPORT_SCHEMA = ReportSchema()
